# Using a load balancer to expose our Kubernetes Kapsule ingress controller service &  shinyproxy

Inspired from : https://www.scaleway.com/en/docs/using-a-load-balancer-to-expose-your-kubernetes-kapsule-ingress-controller-service/


## Objective

In this chapter we will deploy our service in our cluster and configure its access with a load balancer and domain name.


## Prerequisites

Once the k8s kapsule is up and running, download the kubectl config and export it from the console.

The file can be downloaded for scaleway web console in k8s kapsule details. Let's store in 'kubectl/kubeconfig-datapleth-k8s.yaml'

```
export KUBECONFIG=$PWD/kubectl/kubeconfig-datapleth-k8s.yaml
```

Once done we can check the cluster :

```
kubectl get nodes -o wide
```


## Exposing the Ingress Controller Through a Load Balancer Service

### General configuration

By default on Kapsule, these ingress controllers are deployed using a hostPort. This means that the ingress controller will be accessible on all the machines of your cluster on port 80 and 443.

The ingress controller is deployed on the cluster just after the creation of this one.

```
kubectl get ds -n kube-system
NAME                    DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
csi-node                1         1         1       1            1           <none>          22m
kube-proxy              1         1         1       1            1           <none>          22m
nginx-ingress           1         1         1       1            1           <none>          22m
```

By default on Kapsule, a wildcard round-robin DNS record is created, pointing on all your cluster nodes. This means that every time you add or delete a node in your cluster, the DNS record is updated to reflect the state of your nodes.

You can get the FQDN of your cluster in the Scaleway console

```
host essai.aa960809-3e7e-49c0-8a50-374a4c9f70f9.nodes.k8s.fr-par.scw.cloud
```

### Testing with 'cafe-ingress' application

To demonstrate how it works, we will use a test application called [cafe-ingress](https://github.com/nginxinc/kubernetes-ingress/tree/master/examples/complete-example). It’s a pretty simple application just serving different web pages depending on the URL you type.

We firstly create the two services :

```
kubectl create -f 00-lb-ingress-nginx/cafe.yaml
```

To verify the Service’s creation, run the following command:

```
kubectl get service
```

Then we can create the ingress object with 

```
kubectl create -f 00-lb-ingress-nginx/cafe-ingress.yaml 
kubectl get ing
```

The check the service with :

```
curl http://test.aa960809-3e7e-49c0-8a50-374a4c9f70f9.nodes.k8s.fr-par.scw.cloud/coffee
Server address: 100.64.0.8:8080
Server name: coffee-6f4b79b975-f97pg
Date: 21/Feb/2021:13:08:10 +0000
URI: /coffee
Request ID: f2bac3dd93a4f6a4b25e7a25818a369a
```

### Creating the LoadBalancer Service


After using the round-robin DNS records, let us say you prefer to use a load balancer. To do so, you just have to apply this manifest on our cluster.

```
kubectl create -f 00-lb-ingress-nginx/load-balancer.yaml 
```

This load balancer is managed automatically for you by the cloud controller manager. If you want to know more about our cloud controller manager be sure to check out: https://github.com/scaleway/scaleway-cloud-controller-manager

Get the IP address of your newly created load balancer:

```
kubectl get svc -n kube-system

NAME             TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)                      AGE
...
nginx-ingress    LoadBalancer   10.46.225.93    51.159.112.45   80:31185/TCP,443:32679/TCP   23s
```

In this example, your ingress controller is accessible now through its IP address 51.159.112.45. To do so, you have to create a DNS record bound to this address. For the example below, let us say we have an A record for the domain test.datapleth.tolmay.io pointing on this IP address.

```
kubectl create -f 00-lb-ingress-nginx/cafe-ingress-lb.yaml 
kubectl get ing
Warning: extensions/v1beta1 Ingress is deprecated in v1.14+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
NAME              CLASS    HOSTS                                                                  ADDRESS        PORTS   AGE
cafe-ingress      <none>   test.aa960809-3e7e-49c0-8a50-374a4c9f70f9.nodes.k8s.fr-par.scw.cloud   51.15.201.81   80      8m33s
cafe-ingress-lb   <none>   test.datapleth.tolmay.io                                               51.15.201.81   80      21s
```


he application is now accessible using the DNS records pointing to the IP address of your load balancer :

```
host test.datapleth.tolmay.io
test.datapleth.tolmay.io has address 51.159.112.45
curl http://test.datapleth.tolmay.io/coffee
Server address: 100.64.0.9:8080
Server name: coffee-6f4b79b975-8c4cf
Date: 21/Feb/2021:13:22:32 +0000
URI: /coffee
Request ID: 39e716fd1de0d5b93354d7c07c573f58
```

### Wrap-up

We can now list available services and delete these.

```
kubectl get ing
kubectl get svc,nodes
kubectl delete -f 00-lb-ingress-nginx/cafe.yaml 
kubectl delete -f 00-lb-ingress-nginx/cafe-ingress.yaml
kubectl delete -f 00-lb-ingress-nginx/cafe-ingress-lb.yaml
```

The loadbalancer is kept alive otherwise we will have to kill it from the web console.


To recreate the services
```
kubectl create -f 00-lb-ingress-nginx/cafe.yaml 
kubectl create -f 00-lb-ingress-nginx/cafe-ingress-lb.yaml
```


## Adding other services

We will add a simple php docker php app from docker hub : olome/helloworld and a shiny app with simple wordcloud (flaviobarros/shiny-wordcloud).


To recreate the services
```
kubectl create -f 00-lb-ingress-nginx/cafe.yaml 
kubectl create -f 00-lb-ingress-nginx/hello.yaml 
kubectl create -f 00-lb-ingress-nginx/shiny-wordcloud.yaml
kubectl create -f 00-lb-ingress-nginx/cafe-hello-ingress-lb.yaml
```

We fixed the access to static files by adding the right configuration of the  Rewrite annotations (using : https://kubernetes.github.io/ingress-nginx/examples/rewrite/ ).

In the ingress controller annotations `nginx.ingress.kubernetes.io/rewrite-target: /$2` and for each endpoint : `- path: /the-path-i-need(/|$)(.*)`


We have now four services which can be reached with :
http://test.datapleth.tolmay.io/tea/
http://test.datapleth.tolmay.io/coffee/
http://test.datapleth.tolmay.io/hello/
http://test.datapleth.tolmay.io/shiny-wordcloud/

