# Expose services with loadbalancer & domain name (Ingress nginx)

## create load balancer

```
kubectl apply -f load-bancer.yaml
```

You can then check if the Load Balancer is created and its public IP

```
kubectl get svc -n kube-system
NAME             TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)                      AGE
coredns          ClusterIP      10.32.0.10      <none>          53/UDP,53/TCP,9153/TCP       24m
metrics-server   ClusterIP      10.38.31.151    <none>          443/TCP                      24m
nginx-ingress    LoadBalancer   10.44.163.134   51.159.115.95   80:30271/TCP,443:30457/TCP   4m13s
```

You should also see the Load Balancer in scaleway web console and its public IP

## Create a DNS record in scaleway DNS

The you need to make an A record with a wildcard on your domain and check it worked with

```
host test.datapleth.tolmay.io
```
It should return your cluster load balancer IP


## create ingress controller for shinyproxy

As prerequisite, shinyproxy services must be created (see 03) & DNS configured to the public IP of the load balancer.


```
kubectl apply -f ingress-shinyproxy.yaml
```

And then check the ingress controller was created


```
kubectl get ing
```

Enjoy


## Secure access with cert-manager

Inspired from : https://particule.io/blog/scaleway-cert-manager/

### Pre-requisites

On top of tools previously used, we'll need helm v3.
We'll install it from apt (Debian & Ubuntu)

### Install cert-manager

```
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.0.4/cert-manager.yaml
```
### Install Scaleway webhook

```
git clone https://github.com/scaleway/cert-manager-webhook-scaleway.git
cd cert-manager-webhook-scaleway
helm install scaleway-webhook deploy/scaleway-webhook
```

We need now to create a secret. For that we have a template file `issuer_template.yaml` in which we can replace env variable using envsubst and pass it to kubectl

```
envsubst < secret_template.yaml | kubectl apply -f -
```

We can now create the issuer.

```
kubectl apply -f issuer-staging.yaml 
```


### Let's test on a basic application

```
kubectl apply -f cert-test-app/test-application.yaml 
kubectl get events
kubectl get certs
```

You should be able to access the application with https with a warning (as we are on staging).

### Update shinyproxy enabling https

We will proceed with the same approache for shiny proxy, we just have to update
the ingress nginx yaml file to add cert-manager informations

```
kubectl apply -f ingress-shinyproxy-https.yaml
kubectl get certificates -w
kubectl get certs
```

That should work !

