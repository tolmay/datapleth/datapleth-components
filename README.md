# datapleth

Open Source data analytics platform running on Kubernetes

## Introduction & objectives

The objective of this project is to build a data analytics platform based on open source components and running on kubernetes :

- developpement on a minikub configuration
- in a managed kubernetes environmeent such as scaleway kapsule

## Components

For the Kubernetes infrastructure : 

- local kubernetes cluster : [Minikube](https://minikube.sigs.k8s.io/docs/)
- package manager for Kubernetes [helm](https://helm.sh/)
- container registry : [harbor](https://goharbor.io/)

For the analytics platform :
- authentication : [keycloak](https://www.keycloak.org/) (with persistent storage)
- analytics dashboard access (authorization) : [shinyproxy](https://www.shinyproxy.io/)
- kubeflow (https://www.kubeflow.org/docs/about/use-cases/)
    
    requirements: 

    * 4 CPU
    * 50 GB storage
    *12 GB memory

    Stack kubeflow
  
    ![kubeflow stack](./doc/img/kubeflow.svg)
  


![Architecture](./doc/img/overall.png)


## Installation of Minikub & Helm

### Helm3 

Install Helm 3 on Linux | macOS

#### Download Helm 3 installation script.
```￼
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
```
#### Give the script execute permissions.
```
chmod 700 get_helm.sh
```
#### Run the installer.

```
$ ./get_helm.sh
```

Here is a sample installation output:

```
Downloading https://get.helm.sh/helm-v3.4.0-linux-amd64.tar.gz
Verifying checksum... Done.
Preparing to install helm into /usr/local/bin
helm installed into /usr/local/bin/helm
```

#### Confirm installation of Helm 3

```
$ helm version
version.BuildInfo{Version:"v3.4.0", GitCommit:"7090a89efc8a18f3d8178bf47d2462450349a004", GitTreeState:"clean", GoVersion:"go1.14.10"}
```

Enjoy 


### Minikube

Minikube est un outil qui fait tourner un cluster Kubernetes à un noeud unique dans une machine virtuelle sur votre machine.

https://kubernetes.io/fr/docs/tasks/tools/install-minikube/


## Deployment of Minikube and stack components

### Deploy Minikub & deploy harbor

use the deploy script located in this repo : kubernetes/minikube/install_stack.sh

### Deploy keycloak

See 
https://github.com/codecentric/helm-charts/tree/master/charts/keycloak