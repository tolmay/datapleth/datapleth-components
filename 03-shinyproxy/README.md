# containerized ShinyProxy with a Kubernetes cluster


## Objective

In this example, ShinyProxy will run inside a Kubernetes cluster. Shiny containers will also be spawned
in the same cluster. To make the application accessible outside the cluster, a NodePort service is created.

There is two parts

1. installation as nodeport with access from public IP of the cluster
2. set-up a load blancer and an ingress controller to route traffic using domain name



## Pre-requisites

Once the k8s kapsule is up and running, download the kubectl config and export it from the console.

The file can be downloaded for scaleway web console in k8s kapsule details. Let's store in 'kubectl/kubeconfig-datapleth-k8s.yaml'

```
export KUBECONFIG=$PWD/kubectl/kubeconfig-datapleth-k8s.yaml
```

Once done we can check the cluster :

```
kubectl get nodes -o wide
```

We also need to login in the private container registry of our projet on scaleway
```
docker login rg.fr-par.scw.cloud/datapleth -u nologin -p $SCW_SECRET_KEY
```

For the sake of the demo we will push in this private repository a sample shiny analytics app.
We will use it to show how shiny proxy can pull images from private repository using k8s secrets.

```
sudo docker pull openanalytics/shinyproxy-demo
sudo docker tag openanalytics/shinyproxy-demo rg.fr-par.scw.cloud/datapleth/shinyproxy-demo
sudo docker push rg.fr-par.scw.cloud/datapleth/shinyproxy-demo
```

We also need to configure kubernetes secret to enable pull from private repository :

```
kubectl create secret docker-registry registry-secret --docker-server=rg.fr-par.scw.cloud --docker-username=user --docker-password=$SCW_SECRET_KEY --docker-email=user@datapleth.io
kubectl get secret registry-secret --output=yaml
```


## Create shinyproxy services & deployment (nodePort)

### Build `kube-proxy-sidecar` container

From `kube-proxy-sidecar` folder, build & push to your private repo

```
sudo docker build . -t rg.fr-par.scw.cloud/datapleth/kube-proxy-sidecar
sudo docker push rg.fr-par.scw.cloud/datapleth/kube-proxy-sidecar
```

Ensure the `kube-proxy-sidecar` image is available on all your kube nodes.


### Build `shinyproxy-datapleth` container

From `shinyproxy-datapleth`


```
sudo docker build . -t rg.fr-par.scw.cloud/datapleth/shinyproxy-datapleth
sudo docker push rg.fr-par.scw.cloud/datapleth/shinyproxy-datapleth
```

Ensure the `shinyproxy-datapleth` image is available on all your kube nodes.



### Deploy services

Deploy shiny proxy 

```
kubectl apply -f sp-deployment.yaml
```

Run the following command to grant full privileges to the `default` service account which runs the above pod:

```
kubectl create -f sp-authorization.yaml
```

Run the following command to deploy a service exposing ShinyProxy outside the cluster:

```
kubectl create -f sp-service.yaml
```

### Test installation

Get your cluster public IP and the shinyproxy service NodePort with these two commands :

```
kubectl get nodes -o wide
kubectl get svc,nodes
```

You should be able to access shinyproxy from cluster_public_ip:3094

Try to log with jeff / password and test the various apps.


## Set-up LoadBlancer and ingress nginx controller to access shiny proxy with domain name

Follow the [readme](04-lb-ingress-nginx/README.md) in folder 00-lb-infress-nginx folder



## Notes on the configuration

* The `kube-proxy-sidecar` container is used to make the apiserver accessible on `http://localhost:8001` to the `shinyproxy-example` container.

* The service will expose ShinyProxy on all nodes, listening on port `32094`.

* If you do not deploy the service, you can still access ShinyProxy from within the cluster on port `8080`.

* To keep the example consise, the `cluster-admin` role is granted to the `default` service account.
  Best-practice would be to add a dedicated service account and reference it via `serviceAccountName` in the deployment spec.
  The following role is the minimal set of permissions:

  ```yaml
  kind: Role
  apiVersion: rbac.authorization.k8s.io/v1
  metadata:
    namespace: example
    name: example
  rules:
  - apiGroups: [""]
    resources: ["pods", "pods/log"]
    verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
  ```
