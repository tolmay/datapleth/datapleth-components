#!/usr/bin/env bash
#Require the kubeconfig of the kubernetes cluster.

mkdir -p ~/bin
export PATH=$PATH:~/bin

echo "-------install kfctl-----------"
curl -L -o ~/bin/kfctl_v1.2.0-0-gbc038f9_linux.tar.gz https://github.com/kubeflow/kfctl/releases/download/v1.2.0/kfctl_v1.2.0-0-gbc038f9_linux.tar.gz
tar xvf ~/bin/kfctl_v1.2.0-0-gbc038f9_linux.tar.gz -C ~/bin/
rm ~/bin/kfctl_v1.2.0-0-gbc038f9_linux.tar.gz
chmod u+x ~/bin/kfctl
kfctl version
echo "-------install kubeflow-----------"
# Set KF_NAME to the name of your Kubeflow deployment. You also use this
# value as directory name when creating your configuration directory.
# For example, your deployment name can be 'my-kubeflow' or 'kf-test'.
export KF_NAME=kubeflow

# Set the path to the base directory where you want to store one or more
# Kubeflow deployments. For example, /opt/.
# Then set the Kubeflow application directory for this deployment.
export BASE_DIR=${HOME}/kubeflow-cluster
export KF_DIR=${BASE_DIR}/${KF_NAME}

# Set the configuration file to use when deploying Kubeflow.
# The following configuration installs Istio by default.
export CONFIG_URI="https://raw.githubusercontent.com/kubeflow/manifests/v1.0-branch/kfdef/kfctl_k8s_istio.v1.0.2.yaml"

mkdir -p ${KF_DIR}
cd ${KF_DIR}
kfctl apply -V -f ${CONFIG_URI}

kubectl port-forward -n istio-system svc/istio-ingressgateway 8080:80

#Type the following URL in the address bar of your web browser to access the Kubeflow dashboard: http://localhost:8080/