#!/usr/bin/env bash
minikube start --cpus 6 --memory 8G
minikube addons enable ingress
kubectl get nodes

echo " configure helm and tiller "
helm repo update
kubectl config current-context
sleep 40
echo "deploy harbor"
#kubectl create namespace harbor
#helm repo add harbor https://helm.goharbor.io
#git clone https://github.com/goharbor/harbor-helm.git
#cd ../harbor-helm
#helm install harbor harbor/harbor -n harbor
#Label Harbor namespace and copy there the secret with certificates signed by Let's Encrypt certificate:
#sleep 40
echo "Please wait"
#kubectl get ing -n harbor
echo ">>> Add domain name in your hosts file.\n IP Cluster:\n <<<"
minikube ip
###################################################
#               KUBEFLOW                          #
###################################################
echo "Install kubeflow"
echo "!!!!! Download kfctl!!!!!"
cd ..
mkdir kubeflow
cd kubeflow
wget https://github.com/kubeflow/kfctl/releases/download/v1.2.0/kfctl_v1.2.0-0-gbc038f9_linux.tar.gz
tar xvzf kfctl_v1.2.0-0-gbc038f9_linux.tar.gz
rm -f kfctl_v1.2.0-0-gbc038f9_linux.tar.gz

# Add kfctl to PATH, to make the kfctl binary easier to use.
# Use only alphanumeric characters or - in the directory name.
export PATH=$PATH:$PWD/kfctl

# Set the following kfctl configuration file:
export CONFIG_URI="https://raw.githubusercontent.com/kubeflow/manifests/v1.1-branch/kfdef/kfctl_istio_dex.v1.1.0.yaml"

# Set KF_NAME to the name of your Kubeflow deployment. You also use this
# value as directory name when creating your configuration directory.
# For example, your deployment name can be 'my-kubeflow' or 'kf-test'.
export KF_NAME=kf_datapleth

# Set the path to the base directory where you want to store one or more
# Kubeflow deployments. For example, /opt.
# Then set the Kubeflow application directory for this deployment.
export BASE_DIR=$PWD
export KF_DIR=${BASE_DIR}/${KF_NAME}

mkdir -p ${KF_DIR}
cd ${KF_DIR}

# Download the config file and change the default login credentials.
wget -O kfctl_istio_dex.yaml $CONFIG_URI
export CONFIG_FILE=${KF_DIR}/kfctl_istio_dex.yaml
# Credentials for the default user are admin@kubeflow.org:12341234
# To change them, please edit the dex-auth application parameters
# inside the KfDef file.
#vim $CONFIG_FILE
cd .. 
./kfctl apply -V -f ${CONFIG_FILE}


# Kubeflow will be available at localhost:8080
kubectl port-forward svc/istio-ingressgateway -n istio-system 8080:80


