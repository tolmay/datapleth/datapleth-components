#Helm3 

## Install Helm 3 on Linux | macOS
### Download Helm 3 installation script.
```￼
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
```
### Give the script execute permissions.
```
chmod 700 get_helm.sh
```
### Run the installer.
```
$ ./get_helm.sh
```
Here is a sample installation output:
```
Downloading https://get.helm.sh/helm-v3.4.0-linux-amd64.tar.gz
Verifying checksum... Done.
Preparing to install helm into /usr/local/bin
helm installed into /usr/local/bin/helm
```
### Confirm installation of Helm 3
```
$ helm version
version.BuildInfo{Version:"v3.4.0", GitCommit:"7090a89efc8a18f3d8178bf47d2462450349a004", GitTreeState:"clean", GoVersion:"go1.14.10"}
```
Enjoy 


#Minikube

Minikube est un outil qui fait tourner un cluster Kubernetes à un noeud unique dans une machine virtuelle sur votre machine.

https://kubernetes.io/fr/docs/tasks/tools/install-minikube/
